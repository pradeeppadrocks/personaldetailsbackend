const mongoose = require('mongoose');
/** roles
 * 1 - user
 * 2- admin
*/

const register = mongoose.Schema({
    name:String,
    email:String,
    username:{
      type : String,
      index :  true
    },
    password:String,
    role : {
      type : Number,
      default : 1
    }
  });

module.exports = mongoose.model('register', register);