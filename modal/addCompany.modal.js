const mongoose = require('mongoose');

const company = mongoose.Schema({
    company_name:String,
    company_email:String,
    company_location:String,
    usernameref:String,
    roleref:Number

  });

module.exports = mongoose.model('company', company);
// module.exports = mongoose.model('company', company,"company");