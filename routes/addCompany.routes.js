var express = require('express');
var router = express.Router();
var addCompanyController=require('../controllers/addCompany.controllers')



/* GET home page. */

router.get('/addcompany', function(req, res, next) {
    console.log("inside register get")

    addCompanyController.getAllCompany(req.body,function(err,results){
      res.json(results)
    })
  });

router.post('/addcompany', function(req, res, next) {
    console.log("inside register post")
    addCompanyController.addCompany(req.body,function(err,results){
        res.json(results)
      })
});

/* Delete individual  admin */
 router.delete('/addcompany/:_id', function(req, res, next) {
    addCompanyController.deleteCompany(req.params,function(err,results){
       res.json(results)
     })
   });
  
  
module.exports = router;
