var express = require('express');
var router = express.Router();
var registerController=require('../controllers/register.controllers')

router.get('/register/getByUsernameAndEmail/:username/:email', function(req, res, next) {
  console.log("inside register get")
  registerController.findByUserNameAndEmail(req.params,function(err,results){
    res.json(results)
  })
});


/* GET home page. */

router.get('/register', function(req, res, next) {
    console.log("inside register get")
    registerController.getAllAdmin(req.body,function(err,results){
      res.json(results)
    })
  });

  router.get('/register/me', function(req, res, next) {
    console.log("inside me token")
    var token = req.headers['x-access-token'];
    console.log("token----:",token)
    if (!token) return "not provided";
    if(token){
      jwt.verify(token, my_secret_key, function(err, decoded) {
        if (err) return "Error occured"
        console.log("decoded---------:",decoded)
      });
    }
    // registerController.tokenval(req.body,function(err,results){
    //   res.json(results)
    // })
  });

  router.get('/register/:username', function(req, res, next) {
    console.log("inside register post")
    registerController.getUserdetailByUsername(req.params,function(err,results){
        res.json(results)
      })
  });
  router.get('/register/match/aa', function(req, res, next) {
    console.log("inside route match")
    registerController.match(req.params,function(err,results){
      res.json(results)
    })
  });

  router.get('/register/userDetails/:username', function(req, res, next) {
    console.log("inside route lookup")
    registerController.userDetails(req.params,function(err,results){
      res.json(results)
    })
  });

  router.get('/register/role/:role', function(req, res, next) {
    console.log("inside role router");
    registerController.getAdminByRole(req.params,function(err,results){
      res.json(results)
    })
  });

  router.post('/register', function(req, res, next) {
    console.log("inside register post")
    registerController.registerAdmin(req.body,function(err,results){
        res.json(results)
      })
  });

router.put('/register/:id', function(req, res, next) {
  console.log("inside put")
  registerController.update(req,function(err,results){
    res.json(results)
  })
});

/* Delete individual  admin */
router.delete('/register/:_id', function(req, res, next) {
    registerController.deleteAdmin(req.params,function(err,results){
      res.json(results)
    })
  });
 
module.exports = router;