module.exports = {
    formatResponse:function(code, status, data,appName){
        var resObj = {};
        resObj.status = status ? "success" : "error";
        resObj.code=code;
        var contentObj = {};
        contentObj[appName] = data;
        resObj.content = contentObj;
        return resObj;
    }
}

