var register = require('../modal/register.model');
var formatter = require("../utils/formatter");
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt')
let middlewareFunctions ={
    compareAdminPassword:compareAdminPassword,
    getAdminByUsername:getAdminByUsername,
    getRole:getRole,
    createAdminHashPassword:createAdminHashPassword
} 

function compareAdminPassword(candidatePassword, hash, callback) {
    console.log("----------------------",candidatePassword,hash)
    bcrypt.compare(candidatePassword, hash, function (err, isMatch) {
        if (err) throw err;
        callback(null, isMatch);
    });
}
function getAdminByUsername(username, callback) {
    var query = { username: username };
    console.log("query--",query)
    register.findOne(query, callback);
}
function getRole(username, callback) {
    var query = { role: username };
    register.find(query, callback);
}
function createAdminHashPassword(admindata, callback) {
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(admindata.password, salt, function (err, hash) {
            admindata.password = hash;
            admindata.save(callback);
        });
    });
}
module.exports = middlewareFunctions;
