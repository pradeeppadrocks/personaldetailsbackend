var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt')
var company = require('../modal/addCompany.modal');

var registerModel = require('../modal/register.model');

let registerFunctions = {
    addCompany: addCompany,
    deleteCompany:deleteCompany,
    getAllCompany:getAllCompany,
}
//Get all admin
function getAllCompany(data, done) {
    company.find((err, results) => {
        if (err) return done(null, err);
        return done(null, results);
    });
}

//Delete indidual admin by id
function deleteCompany(data, done) {
    try {
        company.findByIdAndRemove(data._id)
            .then(data => {
                return done(null, "data deleted");
            }).catch(err => {
                return done(null, error);
            });
    } catch (error) {
        return done(null, error);
    }
}


function addCompany(data, done) {
    try {
        console.log("inside addCompany 1 ",data)
        const admindata = new company(data);
        admindata.save()
        .then(data => {
                return done(null, data);
            }).catch(err => {
                return done(null, err);
            });
    } catch (err) {
        return done(null, err);
    }
}



module.exports = registerFunctions;