var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt')
var register = require('../modal/register.model');
var company = require('../modal/addCompany.modal');
var middlewareCntrlFunct=require('./middleware.controllersFunc');


var formatter = require("../utils/formatter");

let registerFunctions = {
    registerAdmin: registerAdmin,
    // getAdminByUsername:getAdminByUsername,
    // createAdminHashPassword:createAdminHashPassword,
    getAllAdmin:getAllAdmin,
    deleteAdmin:deleteAdmin,
    getUserdetailByUsername:getUserdetailByUsername,
    getAdminByRole:getAdminByRole,
    update:update,
    userDetails:userDetails,
    match:match,
    tokenval:tokenval,
    findByUserNameAndEmail:findByUserNameAndEmail
    
}
function findByUserNameAndEmail(data,done){
    console.log("data-----",data)
    try {
        register.aggregate(
            [
                //   { "$match": { username: data.username,email:data.email } },
                  { $match: { $or: [ { username:data.username }, { email: data.email } ] } },

            ],
            (err, results) => {
                console.log("Results",results)
                
                if (err) return done(null, formatter.formatResponse(500,false,results,"user"));
                if(!results){
                    return done(null, formatter.formatResponse(500,false,results,"user"));
                }
                return done(null, formatter.formatResponse(200,true,results,"user"));
            }
        );
    } catch (error) {
        return done(null, error);
    }
}


function tokenval(data,done){
    var token = req.headers['x-access-token'];
    if (!token) return "not";
}
/* get combined collections data */
function userDetails(data, done) {
    try {
        register.aggregate(
            [
                { "$match": { username: data.username } },
                //                { "$match": { username: data.username,email:"aaa@gmail.com" } },

                { $lookup:
                    {
                        from: "companies",
                        localField: 'username',
                        foreignField: 'usernameref',
                        as: "personFullDetails"
                    } 
                }
            ],
            (err, results) => {
                if (err) return done(null, formatter.formatResponse(500,false,results,"user"));
                if(!results){
                    return done(null, formatter.formatResponse(500,false,results,"user"));
                }
                return done(null, formatter.formatResponse(200,true,results,"user"));
            }
        );
    } catch (error) {
        return done(null, error);
    }
}

/* get match ny gender */
function match(data, done) {
    try {
        console.log("insie match")
        register.aggregate(
            [
                { "$match": { username: "pradeep" } }
            ],
            (err, results) => {
                if (err) return done(null, err);
                return done(null, results);
            }
        );
    } catch (error) {
        return done(null, error);
    }
}

/* update students by id. */
function update(data, done) {
    console.log("inside update",data.params.id)
    try {
        register.findByIdAndUpdate(
            data.params.id,
            data.body,
            { new: true },
            // the callback function
            (error, data) => {
                // Handle any possible database errors
                if (error) return done(null, formatter.formatResponse(503,false,data,"user"));
                console.log("Sucesss", data)
                return done(null, formatter.formatResponse(200,true,data,"user"));
            }
        )
    } catch (error) {
        return done(null, formatter.formatResponse(500,false,data,"user"));
    }
}
//Get all admin
function getAllAdmin(data, done) {
    console.log("inside register getAllAdmin")
    register.find((err, results) => {
        if (err) return done(null, formatter.formatResponse(500,false,results,"user"));
        return done(null, formatter.formatResponse(201,true,results,"user"));
    });
}
function getAdminByRole(data,done){
    console.log("Role----",data.role);
    if(data.role==2){

        middlewareCntrlFunct.getRole(data.role,function(err,user){
            if(err){
                return done(null,formatter.formatResponse(503,false,user,"admin"));
            }
            if(!user){
                return done(null,formatter.formatResponse(503,false,user,"admin"))
            }
            if(user){
                return done(null,formatter.formatResponse(200,true,user,"amin"))
            }
        })
        // register.find((err, results) => {
        //     if (err) return done(null, err);
        //     return done(null, results);
        // });
    }else if(data.role==1){
        middlewareCntrlFunct.getRole(data.role,function(err,user){
            if(err){
                return done(null,formatter.formatResponse(503,false,user,"user"));
            }
            if(!user){
                return done(null,formatter.formatResponse(503,false,user,"user"))
            }
            if(user){
                return done(null,formatter.formatResponse(200,true,user,"user"))
            }
        })
    }
    else{
        done(null,formatter.formatResponse(503,false,"notfound","user"));
    }
 
}

function getUserdetailByUsername(data,done){
    middlewareCntrlFunct.getAdminByUsername(data.username, function (err, user) {
        if (err) { return done(null, err); }
        if (!user) {
            return done(null, err);
        }
        if (user) {
            return done(null, user);
        }
    });

}
//Delete indidual admin by id
function deleteAdmin(data, done) {
    try {
        register.findByIdAndRemove(data._id)
            .then(data => {
                return done(null, "data deleted");
            }).catch(err => {
                return done(null, error);
            });
    } catch (error) {
        return done(null, error);
    }
}





function getAdminByUsername(username, callback) {
    var query = { username: username };
    console.log("query--",query)
    register.findOne(query, callback);
}
function getRole(username, callback) {
    var query = { role: username };
    register.find(query, callback);
}
function createAdminHashPassword(admindata, callback) {
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(admindata.password, salt, function (err, hash) {
            admindata.password = hash;
            admindata.save(callback);
        });
    });
}



function registerAdmin(data, done) {

    try {
        register.aggregate(
            [
                //   { "$match": { username: data.username,email:data.email } },
                  { $match: { $or: [ { username:data.username }, { email: data.email } ] } },

            ],
            (err, results) => {
                console.log("---------------------------Results:--------------------------",results)
                console.log("length---",results.length)
                if(results.length==0 || !results){
                    console.log("length is zero")
                }
                if(results.length>=1){
                    console.log("length greater then or equal to 1")
                }
                
                if (err) return done(null, formatter.formatResponse(500,false,results,"user"));
                if(results.length==0 || !results){
                    const admindata = new register(data);
                    createAdminHashPassword(admindata, function (err, userdata) {
                        console.log("data--",userdata)
                        if (err) {
                            return done(null, formatter.formatResponse(500,false,userdata,"user"));
                        }else{
                            console.log("inside registerAdmin 3")
                            return done(null,formatter.formatResponse(201,true,userdata,"user"));
                        }
                        
                    });
                   // return done(null, formatter.formatResponse(500,false,results,"user"));
                }
                if (results.length>=1) {
                    console.log("--------------------------:",results[0].username)
                    errMSg="username or email already exists"
                    return done(null, formatter.formatResponse(503,false,errMSg,"user"));
                }
                //return done(null, formatter.formatResponse(200,true,results,"user"));
            }
        );
    } catch (error) {
        return done(null, error);
    }








    // try {
    //     console.log("inside 111111111")
        
    //     getAdminByUsername(data.username, function (err, user) {
    //         console.log("inside 222222",user)

    //         if (err) { return done(null, err); }
    //         if (!user) {
    //             const admindata = new register(data);
    //             createAdminHashPassword(admindata, function (err, data) {
    //                 console.log("data--",data)
    //                 if (err) {
    //                     return done(null, formatter.formatResponse(500,false,data,"user"));
    //                 }else{
    //                     console.log("inside registerAdmin 3")
    //                     return done(null,formatter.formatResponse(201,true,data,"user"));
    //                 }
                    
    //             });
    //         }
          
    //         if (user) {
    //             console.log("user:",user)
    //             errMSg="already exists"
    //             return done(null, formatter.formatResponse(503,false,errMSg,"user"));
    //         }
    //     });
    // } catch (err) {
    //     return done(null, err);
    // }
}



/**
 * 
 function registerAdmin(data, done) {
    try {
        console.log("inside 111111111")
        
        getAdminByUsername(data.username, function (err, user) {
            console.log("inside 222222",user)

            if (err) { return done(null, err); }
            if (!user) {
                const admindata = new register(data);
                createAdminHashPassword(admindata, function (err, data) {
                    console.log("data--",data)
                    if (err) {
                        return done(null, formatter.formatResponse(500,false,data,"user"));
                    }else{
                        console.log("inside registerAdmin 3")
                        return done(null,formatter.formatResponse(201,true,data,"user"));
                    }
                    
                });
            }
          
            if (user) {
                console.log("user:",user)
                errMSg="already exists"
                return done(null, formatter.formatResponse(503,false,errMSg,"user"));
            }
        });
    } catch (err) {
        return done(null, err);
    }
}
 */

module.exports = registerFunctions;